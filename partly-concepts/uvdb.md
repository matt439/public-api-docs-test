---
description: >-
  UVDB simply provides a good structure for linking to parts. When creating
  software, marketplaces, or even internal systems, this is a good scalable
  foundation. There are a few key aspects to UVDB.
---

# UVDB

## Vehicle Base Properties

These are simple properties used to help identify any vehicle. Base properties are things like Makes, Models, Years, Body Codes, Body Types, etc. These are simple resources that are linked to by config fitments, vehicle definitions, and valid configs. See the GraphQL playground for more specific descriptions of each base property. Custom restrictions allow extremely unique restrictions to be added (Seat cover type, has parking sensor, etc).

## Valid Configs

Valid configs contain all the possible valid vehicle base properties this is used to help filter relevant vehicles. For example, after selecting a make, model, and year, you could see all the other valid engine CCs, body types, etc.

## Vehicle Definitions

Vehicle definitions are close approximations to valid configs. There are far fewer, and are usually accurate to around 98%, and higher for common parts such as brakes, filters, etc. Vehicle definitions link to specific base properties. They can link to multiple of the following: Models, Submodels, BodyCodes, EngineDesignations. This is because the same vehicle definition may be sold under a different “Model”, “Submodel”, etc in different regions.

## Regions

Regions are closely linked to countries - examples are USA, Canada, UK, Poland. Each supported region contains the relevant vehicle definitions, and valid configs depending on what vehicles have existed in that region.

## Vehicle Types

Separates cars / trucks, motorcycles, heavy vehicles, etc

## UVDB + GAPC

{% embed url="https://whimsical.com/uvdb-and-gapc-overview-YGTTs2Tehx7XcFufe1oHWX" %}
Putting it all together. This diagram shows how UVDB and GAPC are connected.
{% endembed %}
