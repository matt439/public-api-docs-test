---
description: >-
  GAPC is a global parts catalog. It stores and structures information relating
  to parts in a well-defined, scalable way. Global translations, and
  multi-continent distribution is available via the "i18n
---

# GAPC

## Parts

The most important aspect of a part is the part number, brand, part type, and position. Parts also link to other interchangeable / cross reference parts, attributes, images, and medias. Parts can also be direct "clones" of another part - this is called the parent part.

## Brands

Brands include Aftermarket manufactures (Bosch etc), vehicle OEMs (Ford, Toyota, etc). OEM brands are often grouped, aka "Toyota" and "Lexus" are both mapped to "Toyota Group". Brands may be linked to a parent brand.

## Cross Reference / Interchange

The interchange (often called cross-reference), is used to link parts that are the same, or interchangeable. This is useful for comparing Aftermarket, OEM parts etc. The interchange is very accurate, however can be less accurate for high tolerance parts, or when chamfers etc are important.

## Part Types, Subcategories, and Categories

These three resources are used to categorise parts. This is used to make a category selection, and filter parts. They are structured in the following way

1. **Categories** (top level - Engine, Body, etc)
2. **Subcategories** (2nd level - Bumper, Filter, Hood, etc)
3. **Part Types** (3rd/final level - Bumper Cover, Engine Oil Filter, etc)

## Positions

Positions are specific to each part type, and is essential for finding the correct part. For example part type "Headlight" would also need position "Front Left".

## Part Usage Codes

These codes can be used to exactly define a parts usage. Each part usage code links to a specific category, subcategory, part type, and position. This can be used to link to the part usage that is exactly correct. For example, part code _GCPC1234_ could link to "Exterior Door Handle" → "Front Left"

## Attributes, Images, Media

These resources add extra information about the part. Not all information here is complete.

## UVDB + GAPC

{% embed url="https://whimsical.com/uvdb-and-gapc-overview-YGTTs2Tehx7XcFufe1oHWX" %}
Putting it all together. This diagram shows how UVDB and GAPC are connected.
{% endembed %}
