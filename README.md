---
description: An introduction to the Partly API.
---

# Overview

The Partly API is used to access the most detailed and complete industry-standard fitment data in the world. The API is built using [GraphQL](https://graphql.org/learn/) and gives you full control on how you retrieve our data which is split into our Universal Vehicle Database (UVDB) and Global Auto Parts Catalogue (GAPC).

## UVDB

UVDB simply provides a good structure for linking to parts. When creating software, marketplaces, or even internal systems, this is a good scalable foundation. There is up to 400 attributes per vehicle though it’s worth noting that not all are queryable via the API. [Learn more](partly-concepts/uvdb.md) about UVDB.

## GAPC

GAPC stores and structures information relating to parts in a well-defined, scalable way. It is a database of OEM, OE and Aftermarket part information which includes vehicle compatibility (linking with UVDB) and cross-referenced / interchangeable part numbers. The database includes over 55 million parts. [Learn more](partly-concepts/gapc.md) about GAPC.

## Region support

* **Europe**
* **North America**
* **South America** _(partial)_
* **Australasia** _(partial)_
* **Middle East** _(partial)_

## Talk to us

It is recommended we talk first to confirm your usage is applicable for the Partly API. [Book a time](https://www.partly.com/lets-chat) today to get started with confidence.
