---
description: >-
  Getting started on the Partly API is easy. This quick start guide will take
  you through the steps of querying our data from within your application.
---

# Quick start

## Get your API keys

Authentication for the Partly API is done through API keys. To generate your API keys, use the following steps:

1. Sign in to [**PartsPal**](http://partspal.partly.com)_ (create an account first if needed)_
2. Go to **Settings** -> **Developer Options** -> **API Keys**
3. Press **Generate Keys**
4. Press** Copy key**

You now have your permanent API key for authenticating your app.

## Testing environment

To test the Partly API, you can use the Playground to build queries and see what results are returned. You will need an API key to return any results, this can be set by the following steps:

1. Go to [https://api.partly.com/node-api/graphql](https://api.partly.com/node-api/graphql)
2. Click the **HTTP HEADERS** tab at the bottom left of the screen
3. Paste the following code in **HTTP HEADERS** tab down the bottom using your own public key:

```jsx
{
    "x-api-key":"paste public key here"
}
```

## Running from your application

You can call the Partly API from any application, here are some examples:

{% tabs %}
{% tab title="curl" %}
```
curl '<https://api.partly.com/node-api/graphql>' 
    -H 'Accept-Encoding: gzip, deflate, br'
    -H 'Content-Type: application/json'
    -H 'Accept: application/json'
    -H 'Connection: keep-alive'
    -H 'DNT: 1'
    -H 'Origin: <https://api.partly.com>'
    -H 'x-api-key: paste public key here'
    --data-binary '{"query":"{\\n  gapc {\\n    search_gapc_parts(part_number: \\"26 11 7 511 454 S1\\") {\\n      items {\\n        id\\n        part_number\\n        gapc_brand {\\n          id\\n          name\\n        }\\n        interchange {\\n          items {\\n            part_number\\n            gapc_brand {\\n              id\\n              name\\n            }\\n          }\\n        }\\n        fitted_uvdb_vehicle_definitions {\\n          items {\\n            id\\n            description\\n          }\\n        }\\n      }\\n    }\\n  }\\n}"}'
    --compressed
```

{% hint style="info" %}
Replace "_paste public key here_" with your key
{% endhint %}
{% endtab %}

{% tab title="PHP (using Guzzle)" %}
```
use GuzzleHttp\\Client;

$graphQLquery = ‘{“query”:“{\\n  gapc {\\n    search_gapc_parts(part_number: \\“26 11 7 511 454 S1\\“) {\\n      items {\\n        id\\n        part_number\\n        gapc_brand {\\n          id\\n          name\\n        }\\n        interchange {\\n          items {\\n            part_number\\n            gapc_brand {\\n              id\\n              name\\n            }\\n          }\\n        }\\n        fitted_uvdb_vehicle_definitions {\\n          items {\\n            id\\n            description\\n          }\\n        }\\n      }\\n    }\\n  }\\n}“}’;

$response = (new Client)->request(‘post’, ‘<https://api.partly.com/node-api/graphql>’, [
    ‘headers’ => [
        ‘X-Api-Key’ => ‘paste public key here’,
        ‘Content-Type’ => ‘application/json’
    ],
    ‘body’ => $graphQLquery
]);
```

{% hint style="info" %}
Replace "_paste public key here_" with your key
{% endhint %}
{% endtab %}
{% endtabs %}
